import React, { useState, useEffect } from 'react';
import '../styles/globals.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import NavBar from '../components/NavBar';
import { Container } from 'react-bootstrap';
import AppHelper from '../app-helper.js';


//import Context Provider
import { UserProvider } from '../UserContext';


export default function App({ Component, pageProps }) {
  //state hook for user state, define here for global scope
  const [user, setUser] = useState({
    // Initialized as an object with properties set as null
    // Proper values will be obtained from localStorage AFTER component gets rendered due to Next.JS pre-rendering
  })
  console.log(user)

  
   // localStorage can only be accessed after this component has been rendered, hence the need for an effect hook
   useEffect(() => {
    
    const options = {
        headers: { 
          Authorization: `Bearer ${ AppHelper.getAccessToken() }` ,
         }
    }

    fetch(`http://localhost:4000/api/users/details`, options)
    .then(AppHelper.toJSON)
    .then(data => {
        if (typeof data._id !== 'undefined') {
            setUser({ id: data._id, name: data.email, record: data.record, categories: data.categories })
        } else {
            setUser({ id: null, isAdmin: null })   
        }
    })
}, [user.id])



  //effect hook for testing the setUser() functionality
  useEffect(() => {
    console.log(user.id);
  }, [user.id])

  //function for clearing local storage upon logout
  const unsetUser = () => {
    localStorage.clear();

    //set the user global scope in the context provider  to have its
    //email and isAdmin set to null
    setUser({
      id: null,
      isAdmin: null
    });
  }
  
  return (
      <React.Fragment>
        <UserProvider value={{user, setUser, unsetUser}}>
          <NavBar />
          <Container>
              <Component {...pageProps} />
          </Container>
        </UserProvider>
      </React.Fragment>
  );
  
}
