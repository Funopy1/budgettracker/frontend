import React, { useContext, useState, useEffect } from 'react';
import View from '../../components/View';
import {Row , Col, Button, Table, Container} from 'react-bootstrap'
import Link from 'next/link';
import UserContext from '../../UserContext'

export default function index() {
    
    return ( 
        <View title={ 'Budget Tracker' }>
            <Row className="justify-content-center">
                <Col xs md="12">
                    <h3>Categories</h3>
                    <CategoryForm /> 
                </Col>
            </Row>
        </View>
    )
    
        
}


const CategoryForm = () => {  
    const { user } = useContext(UserContext);
    const [categoryList, setCategoryList] = useState([])
    console.log(user)
    
    
    return (
        <React.Fragment>
            <Link href="/categories/create">
                <Button className="nav-link" role="Button">Add Category</Button>
            </Link> 
            <Container>
            <Table striped bordered hover >
            <thead>
                <tr>
                    <th>Category</th>
                    <th>Type</th>
                </tr>
            </thead>
            <tbody>
                {user.id
                ?
                user.categories.map((category) => { 
                    return(
                        <tr key={category._id}>
                            <td>{category.categoryName}</td>
                            <td>{category.categoryType}</td>
                        </tr> 
                    )
                }) 
                : ""}       
            </tbody>
            </Table>
            </Container>
        </React.Fragment>

    )


}

