import React, {useContext} from 'react'
import { Nav, Navbar} from 'react-bootstrap';
import Link from 'next/link'
import UserContext from '../UserContext';


export default function NavBar() {
    const { user } = useContext(UserContext);

    return (
        <Navbar bg="dark" variant="dark" expand='lg'>


            {(user.id != null)
            ?
            <React.Fragment>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse>
                <Nav className="mr-auto">
                    <React.Fragment>
                        <Link href="/home">
                            <a className="navbar-brand">Budget Tracker</a>
                        </Link>
                        <Link href="/categories">
                            <a className="nav-link" role="button">Categories</a>
                        </Link>
                        <Link href="/record">
                            <a className="nav-link" role="button">Record</a>
                        </Link>
                        <Link href="/trend">
                            <a className="nav-link" role="button">Trend</a>
                        </Link>
                        <Link href="/monthlyIncome">
                            <a className="nav-link" role="button">Monthly Income</a>
                        </Link>
                        <Link href="/monthlyExpenses">
                            <a className="nav-link" role="button">Monthly Expenses</a>
                        </Link>
                        <Link href="/breakdown">
                            <a className="nav-link" role="button">Breakdown</a>
                        </Link>
                        <Link href="/logout">
                                <a className="nav-link" role="button">Logout</a>
                        </Link>
                    </React.Fragment>   
                </Nav>
            </Navbar.Collapse>
            </React.Fragment>
            : 
            <Link href="/">
                <a className="navbar-brand">Budget Tracker</a>
            </Link>
            }
        </Navbar>
        
         
    )
   
}

